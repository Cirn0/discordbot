﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiscordBot
{
    public class BindService
    {
        private BindService() { }

        private static BindService _instance;
        public static BindService Instance
        {
            get
            {
                return _instance ??= new BindService();
            }
        }

        private CommandService commandService;
        public async Task Initialize(CommandService cs)
        {
            commandService = cs;
            await UpdateBinds();
        }

        private readonly List<ModuleInfo> registeredModules = new List<ModuleInfo>();
        public async Task UpdateBinds()
        {
            foreach (var module in registeredModules)
            {
                await commandService.RemoveModuleAsync(module);
            }
            registeredModules.Clear();

            foreach (var item in SQLiteSettings.Instance.GetBinds().GroupBy(a => a.Key.Bind))
            {
                var mi = await commandService.CreateModuleAsync(item.Key, a =>
                {
                    var dic = item.ToDictionary(k => (k.Key.ChannelId, k.Key.UserId), v => v.Value);
                    a.AddCommand("", async (context, objs, serviceProvider, commandInfo) =>
                    {
                        if (dic.TryGetValue((context.Channel.Id, context.User.Id), out string value))
                        {
                            await context.Channel.SendMessageAsync(value, messageReference: context.Message.Reference);
                            try
                            {
                                await context.Message.DeleteAsync();
                            }
                            catch
                            {
                                // Нет прав на удаление чужих сообщений. Ничего не поделать
                            }
                        }
                    },
                    builder =>
                    {
                        builder.WithPriority(9);
                    }).Build(commandService, null);
                });
                registeredModules.Add(mi);
            }
        }
    }

    public class BindCommandTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            if (!String.IsNullOrEmpty(input) && input.StartsWith("!") && input[1..].All(a => Char.IsLetter(a)))
            {
                return Task.FromResult(TypeReaderResult.FromSuccess(new BindCommand { Command = input[1..] }));
            }
            else
            {
                return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "Команда должна начинаться с символа ! и состоять только из букв"));
            }
        }
    }

    public class BindCommand
    {
        public string Command { get; init; }
    }
}
