BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `Winner` (
	`UserName`	TEXT NOT NULL,
	`ChoosingDate`	TEXT NOT NULL,
	`Channel`	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS `Settings` (
	`Name`	TEXT NOT NULL UNIQUE,
	`Value`	TEXT,
	PRIMARY KEY(`Name`)
);
CREATE TABLE IF NOT EXISTS `Rooster` (
	`UserName`	INTEGER,
	`ChoosingDate`	TEXT
);
CREATE TABLE IF NOT EXISTS `Rates` (
	`Object`	TEXT NOT NULL UNIQUE,
	`Rate`	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS `Images` (
	`SearchId`	INTEGER NOT NULL,
	`Path`	TEXT NOT NULL,
	`Tags`	TEXT,
	`Rating`	TEXT,
	`IsUsed`	INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY(`Path`,`SearchId`)
);
CREATE TABLE IF NOT EXISTS `ImageSearches` (
	`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`Tags`	TEXT NOT NULL,
	`Booru`	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS `Greetings` (
	`UserName`	TEXT NOT NULL UNIQUE,
	`GreetingDate`	TEXT NOT NULL,
	PRIMARY KEY(`UserName`)
);
CREATE TABLE IF NOT EXISTS `CommandsUsage` (
	`Command`	TEXT,
	`UserName`	TEXT,
	`DateTime`	TEXT
);
CREATE TABLE IF NOT EXISTS `Binds` (
	`ChannelId`	TEXT NOT NULL,
	`UserId`	TEXT NOT NULL,
	`Bind`	TEXT NOT NULL,
	`Content`	TEXT,
	PRIMARY KEY(`UserId`,`Bind`,`ChannelId`)
);
CREATE TRIGGER insert_image 
after insert on Images
begin
	update Images set IsUsed = case when ((select max(IsUsed) from Images where Path = NEW.Path) = 1) then 1 else 0 end where SearchId = NEW.SearchId and Path = NEW.Path;
end;
COMMIT;
