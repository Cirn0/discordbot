﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace DiscordBot
{
    class Program
    {
        DiscordSocketClient client;
        CommandService cs;

        static async Task Main(string[] args)
        {
            var loc = Assembly.GetExecutingAssembly().Location;
            Directory.SetCurrentDirectory(Path.GetDirectoryName(loc));
            await new Program().MainAsync();
        }

        public async Task MainAsync()
        {
            client = new DiscordSocketClient(new DiscordSocketConfig
            {
                AlwaysDownloadUsers = true
            });

            client.Log += Log;

            string token = SQLiteSettings.Instance.GetSettings("token");
            await client.LoginAsync(TokenType.Bot, token);
            await client.StartAsync();

            client.MessageReceived += Client_MessageReceived;

            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;

            cs = new CommandService(new CommandServiceConfig { CaseSensitiveCommands = false, DefaultRunMode = RunMode.Async, IgnoreExtraArgs = false, SeparatorChar = ' ' });
            cs.AddTypeReader<BindCommand>(new BindCommandTypeReader());
            cs.AddTypeReader<ReminderTimeSpan>(new ReminderTimeSpanTypeReader());
            await cs.AddModuleAsync<CommandModule>(null);
            await BindService.Instance.Initialize(cs);
            cs.CommandExecuted += Cs_CommandExecuted;

            await Task.Delay(-1);
        }

        private async Task Cs_CommandExecuted(Optional<CommandInfo> arg1, ICommandContext arg2, IResult arg3)
        {
            if (!arg3.IsSuccess && arg3.Error != CommandError.UnknownCommand)
            {
                await arg2.Channel.SendMessageAsync($"Некорректный вызов команды: {arg3.ErrorReason}");
            }
        }

        private static readonly string[] shortCommand = { "rate", "stop", "help", "choose", "sleep", "wakeup", "hype", "remind", "rand", "bind", "shame" };
        private static readonly RequireOwnerAttribute attrib = new RequireOwnerAttribute();
        private async Task Client_MessageReceived(SocketMessage arg)
        {
            if (arg is not SocketUserMessage message)
            {
                return;
            }
            var context = new SocketCommandContext(client, message);

            
            if (CheckLocked() && !(await attrib.CheckPermissionsAsync(context, null, null)).IsSuccess)
            {
                return;
            }
            if (SQLiteSettings.Instance.ShouldGreet(message.Author.Username) && !message.Author.IsBot)
            {
                SQLiteSettings.Instance.SetGreetingDate(message.Author.Username);
                int hour = DateTime.Now.ShiftToNskTimeZone().Hour;
                string prefix = hour < 12 ? "Доброе утро" :
                                            hour < 18 ? "Добрый день" :
                                                        "Привет";
                await message.Channel.SendMessageAsync($"{prefix}, {message.Author.Mention}");
            }
            if (message.MentionedUsers.Select(a => a.Id).Contains(client.CurrentUser.Id) && 
                message.Content.EndsWith('?') && 
                (await attrib.CheckPermissionsAsync(context, null, null)).IsSuccess)
            {
                await message.Channel.SendMessageAsync("Да, вы правы.");
                return;
            }

            if (message.Content.Contains("кристин", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.Channel.SendFileAsync(@"tina.jpg");
                return;
            }

            int argPos = 0;
            if (message.HasCharPrefix('!', ref argPos))
            {
                IDisposable typingState = null;
                if (shortCommand.All(a => !message.Content.StartsWith($"!{a}", StringComparison.InvariantCultureIgnoreCase)))
                {
                    typingState = context.Channel.EnterTypingState(new RequestOptions { Timeout = null });
                }
                try
                {
                    var res = await cs.ExecuteAsync(context, argPos, null);
                }
                finally
                {
                    typingState?.Dispose();
                }
            }
        }

        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            Log(e.Exception);
        }

        private Task Log(LogMessage msg)
        {
            System.IO.File.AppendAllText("DiscordBotLog.log", "\r\n" + msg.ToString());
            return Task.CompletedTask;
        }

        private static void Log(Exception e)
        {
            System.IO.File.AppendAllText("DiscordBotLog.log", "\r\n" + e.ToString());
        }

        private static bool CheckLocked()
        {
            return SQLiteSettings.Instance.GetSettings("locked") == "1";
        }
    }
}
