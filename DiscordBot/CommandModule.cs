﻿using Discord;
using Discord.Audio;
using Discord.Commands;
using Discord.Rest;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DiscordBot
{
    public class CommandModule : ModuleBase<SocketCommandContext>
    {
        private static readonly Random rng = new Random();

        private static readonly string[] prohibitedUnsafeTags = { "makise_kurisu" };

        private static readonly Dictionary<Boorus, IBooruApiHelper> apiHelpers = new Dictionary<Boorus, IBooruApiHelper>
        {
            { Boorus.Safebooru, new SafebooruApiHelper() },
            { Boorus.TBIB, new TBIBApiHelper() },
            { Boorus.Derpibooru, new DerpibooruApiHelper() }
        };

        [Command("shame")]
        [Priority(1)]
        [RequireOwner]
        public async Task ShameCommand()
        {
            IAsyncEnumerable<IReadOnlyCollection<IMessage>> messages = Context.Channel.GetMessagesAsync(100);

            var enumerator = messages.GetAsyncEnumerator();
            while (await enumerator.MoveNextAsync())
            {
                var curr = enumerator.Current;
                foreach (var msg in curr)
                {
                    if (msg.Author.Id == Context.Client.CurrentUser.Id)
                    {
                        await msg.DeleteAsync();
                        await Context.Message.DeleteAsync();
                        return;
                    }
                }
            }

            await Context.Channel.SendMessageAsync("Не понимаю, за что мне должно быть стыдно.");
            return;
        }

        [Command("image")]
        [Priority(1)]
        public Task ImageCommand([Remainder] string tags)
        {
            return DoPostImage(Context, tags, Boorus.TBIB);
        }

        [Command("safeimage")]
        [Priority(1)]
        public Task SafeImageCommand([Remainder] string tags)
        {
            return DoPostImage(Context, tags, Boorus.Safebooru);
        }

        [Command("ponyimage")]
        [Priority(1)]
        public Task PonyImageCommand([Remainder] string tags)
        {
            return DoPostImage(Context, tags, Boorus.Derpibooru);
        }

        [Command("hi")]
        [Priority(1)]
        public Task HiCommand()
        {
            return DoPostImage(Context, "makise_kurisu solo", Boorus.Safebooru, "Привет!");
        }

        private static bool currentlyInGame;
        [Command("game")]
        [Priority(1)]
        public async Task GameCommand(string args = null)
        {
            if (currentlyInGame)
            {
                return;
            }
            try
            {
                currentlyInGame = true;
                ulong channelId = Context.Channel.Id;
                if ("stats".Equals(args, StringComparison.InvariantCultureIgnoreCase))
                {
                    var stats = SQLiteSettings.Instance.GetWinnerStats(channelId);
                    StringBuilder sb = new StringBuilder();
                    for (int place = 0; place < stats.Count; ++place)
                    {
                        if (place == 0)
                        {
                            var tenryu = (Context.Channel as SocketTextChannel).Guild.Emotes.SingleOrDefault(a => a.Name == "tenryu");
                            var rmuscle = (Context.Channel as SocketTextChannel).Guild.Emotes.SingleOrDefault(a => a.Name == "rmuscle");
                            sb.AppendLine($"Текущий лидер: {stats[place].Item1} с {stats[place].Item2} победами. {(tenryu == null || rmuscle == null ? "" : $":muscle: {tenryu} {rmuscle}")}");
                        }
                        else
                        {
                            sb.AppendLine($"{place + 1} место: {stats[place].Item1} с {stats[place].Item2} победами.");
                        }
                    }
                    await Context.Channel.SendMessageAsync(sb.ToString());
                    return;
                }
                Winner winner = SQLiteSettings.Instance.GetWinner(channelId);

                if (winner != null)
                {
                    await Context.Channel.SendMessageAsync($"На данный момент хохол - {winner.User}. До перевыборов осталось {winner.RemainingTime} минут.");
                    return;
                }

                var oldStats = SQLiteSettings.Instance.GetWinnerStats(channelId);

                await Context.Channel.SendMessageAsync("Увлекательная игра \"Кто хохол?\" начинается!");
                var users = (Context.Channel as SocketGuildChannel).Guild.Users.Where(a => !a.IsBot);
                int random = rng.Next(users.Count());
                string nick = users.ToArray()[random].Username;
                await Task.Delay(2500);
                SQLiteSettings.Instance.SetWinner(nick, channelId);
                var currentStats = SQLiteSettings.Instance.GetWinnerStats(channelId);

                int prevIndex = oldStats.Select(a => a.Item1).ToList().IndexOf(nick);
                int currIndex = currentStats.Select(a => a.Item1).ToList().IndexOf(nick);
                RestUserMessage msg = null;
                string mainMessage = $"И хохол: {nick}";
                if (currIndex != prevIndex)
                {
                    msg = await Context.Channel.SendMessageAsync($"{mainMessage}! Благодаря этому он поднимается на {currIndex + 1} место. Поздравляем!");
                }
                else
                {
                    msg = await Context.Channel.SendMessageAsync($"{mainMessage}! Поздравим его!");
                }


                await msg.AddReactionAsync(new Emoji("🎊"), new RequestOptions());

                var first = currentStats.First();
                if (first.Item2 >= 50)
                {
                    await Context.Channel.SendFileAsync(@"hug.jpg", $"И у нас есть Бандера: {first.Item1}! Поздравим его!");
                    SQLiteSettings.Instance.SetSettings($"WinnerBeginDate_{channelId}", DateTime.Now.ToString("yyyyMMddHHmmss"));
                }
                return;
            }
            finally
            {
                currentlyInGame = false;
            }
        }

        private static readonly List<string> webms = new List<string>();
        [Command("webm")]
        [Priority(1)]
        public async Task WebmCommand()
        {
            //string[] files = Directory.EnumerateFiles("D:\\DiscordBot\\webm").Where(a => a.EndsWith(".webm") || a.EndsWith(".mp4")).Except(webms).ToArray();
            //if (!files.Any())
            //{
            //    webms.Clear();
            //    files = Directory.EnumerateFiles("D:\\DiscordBot\\webm").Where(a => a.EndsWith(".webm") || a.EndsWith(".mp4")).ToArray();
            //}
            //if (!files.Any())
            //{
            //    await Context.Channel.SendMessageAsync("Не найдено ни одного подходящего файла.");
            //    return;
            //}
            //string file = files[rng.Next(files.Length)];
            //webms.Add(file);
            //await Context.Channel.SendFileAsync(file);
            return;
        }

        [Command("rate")]
        [Priority(1)]
        public async Task RateCommand([Remainder] string obj)
        {
            if (obj == null)
            {
                await Context.Channel.SendMessageAsync("Правильный синтаксис: \"!rate что-нибудь\", например !rate твой интеллект");
                return;
            }

            string rate = RateString(obj);

            await Context.Channel.SendMessageAsync($"Я оцениваю {obj} на {rate} из 10.");
            return;
        }

        private string RateString(string obj)
        {
            string rate = SQLiteSettings.Instance.GetRate(obj);
            if (rate == null)
            {
                rate = rng.Next(1, 11).ToString();
                SQLiteSettings.Instance.SetRate(obj, rate);
            }

            return rate;
        }

        private static readonly Dictionary<ulong, CancellationTokenSource> playingCancellationTokens = new Dictionary<ulong, CancellationTokenSource>();
        private const int BUFFER_SIZE = 2500;
        [Command("play")]
        [Priority(1)]
        public async Task PlayCommand([Remainder] string link)
        {
            var voiceChannel = Context.Guild.VoiceChannels.FirstOrDefault(a => a.Users.Any(a => a.Id == Context.User.Id));
            ulong channelId = Context.Channel.Id;

            if (playingCancellationTokens.ContainsKey(channelId))
            {
                await Context.Channel.SendMessageAsync($"Извините, сейчас не могу.");
                return;
            }

            Process p = null;
            Process p1 = null;

            var token = new CancellationTokenSource();
            playingCancellationTokens.Add(channelId, token);
            token.Token.Register(() =>
            {
                if (p != null)
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                    try
                    {
                        p.Close();
                    }
                    catch { }
                }
                if (p1 != null)
                {
                    try
                    {
                        p1.Kill();
                    }
                    catch { }
                    try
                    {
                        p1.Close();
                    }
                    catch { }
                }
            });

            string audioFile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + ".wav");

            string videoFile = $"{link}.mp4";
            foreach (var ch in Path.GetInvalidFileNameChars())
            {
                videoFile.Replace(ch, '_');
            }
            var dir = Path.Combine("DiscordBot", "yt");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            videoFile = Path.Combine(dir, videoFile);

            IAudioClient client = null;

            try
            {
                if (!File.Exists(audioFile))
                {
                    ProcessStartInfo psi1 = new ProcessStartInfo
                    {
                        FileName = "youtube-dl",
                        Arguments = $"-i -o {videoFile} {link} --geo-bypass -q"
                    };
                    p1 = Process.Start(psi1);
                    p1.WaitForExit();
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }
                }

                ProcessStartInfo psi = new ProcessStartInfo
                {
                    FileName = "ffmpeg-ytdl",
                    Arguments = $"-xerror -y -i \"{videoFile}\" -f s16le -vn -ar 48000 {audioFile}"
                };
                p = Process.Start(psi);
                p.WaitForExit();
                if (token.IsCancellationRequested)
                {
                    return;
                }

                SocketVoiceChannel vc = voiceChannel ?? (Context.Channel as SocketGuildChannel).Guild.VoiceChannels.First();
                client = await vc.ConnectAsync();
                using AudioOutStream discord = client.CreatePCMStream(AudioApplication.Music, null, BUFFER_SIZE);
                byte[] arr = File.ReadAllBytes(audioFile);
                for (int i = 0; i < arr.Length / 1024; i++)
                {
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }
                    await discord.WriteAsync(arr, i * 1024, 1024);
                }
                await Task.Delay(BUFFER_SIZE + client.Latency);
            }
            catch (InvalidOperationException)
            {
                return;
            }
            finally
            {
                playingCancellationTokens.Remove(channelId);
                File.Delete(audioFile);
                if (client != null)
                {
                    await client.StopAsync();
                }
            }
            return;
        }

        [Command("stop")]
        [Priority(1)]
        public Task StopCommand()
        {
            ulong channelId = Context.Channel.Id;
            if (playingCancellationTokens.TryGetValue(channelId, out var value))
            {
                value.Cancel();
                playingCancellationTokens.Remove(channelId);
            }

            return Task.CompletedTask;
        }

        [Command("facepalm")]
        [Priority(1)]
        public async Task FacepalmCommand()
        {
            //string[] files = Directory.EnumerateFiles("D:\\DiscordBot\\Facepalm").ToArray();
            //if (!files.Any())
            //{
            //    await Context.Channel.SendMessageAsync("Не найдено ни одного подходящего файла.");
            //    return;
            //}
            //string file = files[rng.Next(files.Length)];
            //await Context.Channel.SendFileAsync(file);
            return;
        }

        private static readonly string[] chooseSentence = new[] { "Мне кажется, будет лучше \"{0}\".", "Наверное, \"{0}\" будет лучше.", "Попробуйте \"{0}\".", "Советую вам \"{0}\"." };
        [Command("choose")]
        [Priority(1)]
        public async Task ChooseCommand([Remainder] string args)
        {
            string[] variants = args.Split(new[] { " или ", "," }, StringSplitOptions.RemoveEmptyEntries);
            if (variants.Length < 2)
            {
                await Context.Channel.SendMessageAsync("Маловато вариантов для выбора");
                return;
            }

            string result = variants.Select(a => a.Trim())
                                    .Select(a => (Source: a, Rate: RateString(a)))
                                    .Select(a => (Source: a.Source, Rate: Int32.TryParse(a.Rate, out int result) ? result : 11))
                                    .OrderByDescending(a => a.Rate)
                                    .ThenBy(a => a.Source)
                                    .First()
                                    .Source;

            string sentence = chooseSentence[rng.Next(chooseSentence.Length)];

            await Context.Channel.SendMessageAsync(String.Format(sentence, result));
            return;
        }

        [Command("help")]
        [Priority(1)]
        public async Task HelpCommand()
        {
            await Context.Channel.SendMessageAsync("Список команд:\r\n" +
                                    "!image <tag> - поиск изображения на TBIB по тэгу\r\n" +
                                    "!safeimage <tag> - поиск изображения на safebooru по тэгу\r\n" +
                                    "!ponyimage <tag> - поиск изображения на derpibooru по тэгу\r\n" +
                                    "!game - увлекательная игра \"Кто красавчик?\" (аргумент stats для текущих результатов)\r\n" +
                                    //"!webm - случайная webm-ка\r\n" +
                                    "!hi - поздороваться\r\n" +
                                    "!rate <smth> - оценить что-то по десятибальной шкале\r\n" +
                                    "!play <link> - воспроизвести в голосовом чате музыку с ютуба\r\n" +
                                    "!stop - остановить воспроизведение музыки с ютуба\r\n" +
                                    //"!facepalm - фейспалм\r\n" +
                                    "!choose <vars> - выбор из нескольких вариантов\r\n" +
                                    "!hype - немножечко хайпануть\r\n" +
                                    "!remind <H>h<M>m сообщение - напомнить о чём-либо через <H> часов <M> минут\r\n" +
                                    "!rand <begin> <end> - получить случайное целое число от <begin> до <end> (границы включаются)\r\n" +
                                    "!bind !<command> <value> - если пользователь запостит сообщение, состоящее из !<command>, бот удалит его и запостит <value>\r\n" +
                                    "!bruh - bruh\r\n" +
                                    "!duel - дуэль с указанными людьми");
        }

        [Command("hype")]
        [Priority(1)]
        public async Task HypeCommand()
        {
            await Context.Channel.SendMessageAsync("Люблю хайп:fire::fire: и все что с ним связано:stuck_out_tongue_winking_eye::stuck_out_tongue_winking_eye:, музыка элджей :two_hearts::two_hearts::sunglasses::ok_hand:лил пип:heart_eyes::heart_eyes::astonished: пошлая молли :smiling_imp::smiling_imp: гспд рейв эпидемия) :laughing::laughing:\r\nШмот вэнсы :heartpulse::heartpulse: толстовка трешер :shirt::bow:\r\nОбычно я хайплю :scream_cat::scream_cat:, это мое хобби :joy_cat::joy_cat:");
        }

        [Command("sleep")]
        [Priority(1)]
        [RequireOwner]
        public async Task SleepCommand()
        {
            foreach (var ch in Context.Client.Guilds.Select(a => a.Channels.OfType<SocketTextChannel>().FirstOrDefault()))
            {
                await ch.SendMessageAsync("Ушла спать. На запросы не отвечаю. Просьба не будить.");
            }
            SQLiteSettings.Instance.SetSettings("locked", "1");
        }

        [Command("wakeup")]
        [Priority(1)]
        [RequireOwner]
        public async Task WakeUpCommand()
        {
            foreach (var ch in Context.Client.Guilds.Select(a => a.Channels.OfType<SocketTextChannel>().FirstOrDefault()))
            {
                await ch.SendMessageAsync("Здравствуйте. Можете задавать свои вопросы.");
            }
            SQLiteSettings.Instance.SetSettings("locked", "0");
        }

        [Command("remind")]
        [Priority(1)]
        public async Task RemindCommand(ReminderTimeSpan timeSpan, [Remainder] string message)
        {
            ReminderService.Instance.Remind(Context, DateTime.Now + timeSpan.Time, message);
            await Context.Channel.SendMessageAsync($"Хорошо. Я вам напомню через {FormatTimeString(timeSpan.Time)}");
        }

        [Command("rand")]
        [Priority(1)]
        public async Task RandCommand(int first, int second)
        {
            await Context.Channel.SendMessageAsync($"Ну, допустим, {rng.Next(Math.Min(first, second), Math.Max(first, second) + 1)}.");
            return;
        }

        [Command("bind")]
        [Priority(1)]
        public async Task BindCommand(BindCommand command, [Remainder] string message)
        {
            SQLiteSettings.Instance.AddBind(Context.Channel.Id, Context.User.Id, command.Command, message);
            await BindService.Instance.UpdateBinds();
        }

        [Command("bruh")]
        [Priority(1)]
        public async Task BruhCommand()
        {
            await Context.Channel.SendFileAsync("bruh.jpg");
            return;
        }

        [Command("duel")]
        [Priority(1)]
        public async Task DuelCommand([Remainder] string message = null)
        {
            await DuelService.Instance.ProcessDuel(Context);
        }

        private string GetWinner(ulong channel)
        {
            var participants = Duels[channel].participants.Where(a => a.Value).ToList();
            if (participants.Count == 1)
            {
                return $"Все остальные струсили, поэтому победитель - {MentionUtils.MentionUser(participants[0].Key)}";
            }
            else
            {
                var winner = participants[rng.Next(participants.Count)];
                var mention = MentionUtils.MentionUser(winner.Key);
                if (participants.Count > 2)
                {
                    return $"Последний оставшийся в живых - {mention}";
                }
                else
                {
                    return $"В перестрелке победил {mention}";
                }
            }
        }

        private static Dictionary<ulong, (DateTime dueDate, Dictionary<ulong, bool> participants)> Duels { get; set; } = new();


        #region helpers

        private static async Task DoPostImage(SocketCommandContext context, string tags, Boorus booru, string message = null)
        {
            var channel = context.Channel;
            if (prohibitedUnsafeTags.Any(a => tags.Contains(a)) && booru != Boorus.Safebooru && !tags.Contains("rating:safe"))
            {
                var msg = await channel.SendMessageAsync("Ты это что такое ищешь? Этот тег можно искать только с \"rating:safe\".");
                await msg.AddReactionAsync(new Emoji("💢"));
                return;
            }
            Image? img = SQLiteSettings.Instance.GetImage(tags, booru);
            while (img == null)
            {
                List<Image> images = GetImages(tags, booru);
                if (images.Count == 0)
                {
                    await channel.SendMessageAsync("Не найдено ни одного изображения по тегу");
                    return;
                }
                SQLiteSettings.Instance.AddImages(tags, images, booru);
                img = SQLiteSettings.Instance.GetImage(tags, booru);
            }

            string href = img.Value.Path;
            Console.WriteLine(href);
            byte[] image = HttpRequestHelper.GetResponse(href, apiHelpers[booru].Anonymous);

            string tempFile = Path.Combine(Path.GetTempPath(), Path.GetFileName(href));
            try
            {
                File.WriteAllBytes(tempFile, image);

                await context.Message.DeleteAsync();
                await PostImageOnChannel(channel, tempFile, message ?? $"{tags}\r\n\r\n{img.Value.Tags}", !String.Equals(img.Value.Rating, "safe", StringComparison.OrdinalIgnoreCase));
            }
            finally
            {
                File.Delete(tempFile);
            }
        }

        private static readonly Dictionary<(string, Boorus), int> loadedPages = new Dictionary<(string, Boorus), int>();
        private static List<Image> GetImages(string tag, Boorus booru)
        {
            List<Image> imagesList = new List<Image>();
            int page = 0;
            var key = (tag, booru);
            if (loadedPages.ContainsKey(key))
            {
                page = ++loadedPages[key];
                loadedPages.Remove(key);
            }
            loadedPages.Add(key, page);

            List<Image> images = apiHelpers[booru].GetImages(tag, page);

            return images.Where(a => a.Rating.Equals("safe") || !prohibitedUnsafeTags.Any(b => a.Tags.Contains(b))).ToList();
        }

        private static async Task PostImageOnChannel(ISocketMessageChannel channel, string filePath, string message, bool isUnsafe)
        {
            if (await TryPostOnChannel())
            {
                return;
            }

            //string url = "https://cirn0.xyz/api/upload";
            //using (var webClient = new WebClient())
            //{
            //    var ret = webClient.UploadFile(url, filePath);
            //    string outFileName = Encoding.Default.GetString(ret);
            //    bool uploaded = false;
            //    for (int i = 0; i < 3; ++i)     //иногда по неизвестным причинам с первого раза не заливается, поэтому пытаемся починить вот так
            //    {
            //        try
            //        {
            //            string href = $"https://cirn0.xyz/{outFileName}";
            //            if (isUnsafe)
            //            {
            //                href = $"||{href}||";
            //            }
            //            await channel.SendMessageAsync(message + "\r\n" + href, options: new RequestOptions { Timeout = null });
            //            uploaded = true;
            //            break;
            //        }
            //        catch
            //        {
            //            await Task.Delay(1000);
            //        }
            //    }
            //    if (!uploaded)
            //    {
                    await channel.SendFileAsync(@"cant_upload.jpg", $"Извините, не удалось загрузить изображение.");
            //    }
            //}

            async Task<bool> TryPostOnChannel()
            {
                try
                {
                    await channel.SendFileAsync(filePath, message, isSpoiler: isUnsafe);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        private static string FormatTimeString(TimeSpan ts)
        {
            string s = String.Empty;
            int h = (int)ts.TotalHours;
            if (h != 0)
            {
                if (h % 100 >= 10 && h % 100 <= 20)
                {
                    s += $"{h} часов ";
                }
                else if (h % 10 == 1)
                {
                    s += $"{h} час ";
                }
                else if (h % 10 >= 2 && h % 10 <= 4)
                {
                    s += $"{h} часа ";
                }
                else
                {
                    s += $"{h} часов ";
                }
            }
            int m = ts.Minutes;
            if (m != 0)
            {
                if (m % 100 >= 10 && m % 100 <= 20)
                {
                    s += $"{m} минут";
                }
                else if (m % 10 == 1)
                {
                    s += $"{m} минуту";
                }
                else if (m % 10 >= 2 && m % 10 <= 4)
                {
                    s += $"{m} минуты";
                }
                else
                {
                    s += $"{m} минут";
                }
            }

            return s.Trim();
        }

        #endregion
    }
}
