﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace DiscordBot
{
    public class ReminderService
    {
        private ReminderService()
        {
            Task.Factory.StartNew(InnerProcessingTask, TaskCreationOptions.LongRunning);
        }

        private static ReminderService _instance;
        public static ReminderService Instance
        {
            get
            {
                return _instance ??= new ReminderService();
            }
        }

        private readonly List<(SocketCommandContext context, DateTime when, string message)> reminders = new();

        public void Remind(SocketCommandContext context, DateTime when, string message)
        {
            reminders.Add((context, when, message));
            cts.Cancel();
        }

        CancellationTokenSource cts = new CancellationTokenSource();
        private async Task InnerProcessingTask()
        {
            while (true)
            {
                cts = new CancellationTokenSource();
                var val = reminders.OrderBy(a => a.when).FirstOrDefault();
                try
                {
                    if (val == default || val.when < DateTime.Now)
                    {
                        await Task.Delay(-1, cts.Token);
                    }
                    else
                    {
                        await Task.Delay(val.when - DateTime.Now, cts.Token);
                    }
                }
                catch (OperationCanceledException)
                {
                    continue;
                }

                await val.context.Channel.SendMessageAsync($"{val.context.User.Mention}, вы просили напомнить: \r\n{val.message}");
                reminders.Remove(val);
            }
        }
    }

    public class ReminderTimeSpanTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            try
            {
                var ts = ParseTimeString(input);
                if (ts == TimeSpan.Zero)
                {
                    throw null;
                }
                return Task.FromResult(TypeReaderResult.FromSuccess(new ReminderTimeSpan { Time = ts }));
            }
            catch
            {
                return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "Не удалось выяснить, через сколько нужно напомнить"));
            }
        }

        private static TimeSpan ParseTimeString(string str)
        {
            string hours = Regex.Match(str, @"\d+[hч]").Value;
            if (hours.Length > 0)
            {
                hours = hours[0..^1];
            }
            else
            {
                hours = "0";
            }
            string minutes = Regex.Match(str, @"\d+[mм]").Value;
            if (minutes.Length > 0)
            {
                minutes = minutes[0..^1];
            }
            else
            {
                minutes = "0";
            }

            return new TimeSpan(Int32.Parse(hours), Int32.Parse(minutes), 0);
        }
    }

    public class ReminderTimeSpan
    {
        public TimeSpan Time { get; init; }
    }
}
