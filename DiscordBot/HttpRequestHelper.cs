﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace DiscordBot
{
    static public class HttpRequestHelper
    {
        static public byte[] GetResponse(string url, bool anonymous)
        {
            if (anonymous)
            {
                return GetResponseThroughProxy(url);
            }
            else
            {
                return GetResponseDirectly(url);
            }
        }

        static public byte[] GetResponseDirectly(string url)
        {
            HttpWebRequest req = WebRequest.CreateHttp(url);
            req.Proxy = WebRequest.DefaultWebProxy;
            req.UserAgent = "Other";
            req.Method = WebRequestMethods.Http.Get;
            using (var resp = req.GetResponse())
            {
                using (MemoryStream sr = new MemoryStream())
                {
                    resp.GetResponseStream().CopyTo(sr);
                    return sr.ToArray();
                }
            }
        }

        static public byte[] GetResponseThroughProxy(string url_)
        {
            HttpWebRequest req = WebRequest.CreateHttp(url_);
            req.Proxy = new WebProxy("proxy-nossl.antizapret.prostovpn.org", 29976);
            req.UserAgent = "Other";
            req.Method = WebRequestMethods.Http.Get;
            using (var resp = req.GetResponse())
            {
                using (MemoryStream sr = new MemoryStream())
                {
                    resp.GetResponseStream().CopyTo(sr);
                    return sr.ToArray();
                }
            }
        }

        static public byte[] OldGetResponseThroughProxy(string url_)
        {
            // TODO: xameleo.xyz больше не работает с TBIB и прочими подобными сайтами
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp("http://cameleo.xyz/r");
                hwr.Proxy = WebRequest.DefaultWebProxy;
                hwr.UserAgent = "Other";
                hwr.Method = WebRequestMethods.Http.Post;
                hwr.AllowAutoRedirect = true;
                hwr.Referer = "http://cameleo.xyz";
                hwr.Headers.Add("Origin", hwr.Referer);
                hwr.Headers.Add("Upgrade-Insecure-Requests", "1");
                hwr.Host = "cameleo.xyz";
                hwr.ContentType = "application/x-www-form-urlencoded";
                hwr.CookieContainer = new CookieContainer(4);
                hwr.CookieContainer.Add(new Cookie("uechat_25441_pages_count", "13", "/", "cameleo.xyz"));
                hwr.CookieContainer.Add(new Cookie("uechat_25441_first_time", "1538623482491", "/", "cameleo.xyz"));
                hwr.CookieContainer.Add(new Cookie("uechat_25441_disabled", "true", "/", "cameleo.xyz"));
                hwr.CookieContainer.Add(new Cookie("uechat_25441_mode", "0", "/", "cameleo.xyz"));


                string url = CorrectLink(url_);
                byte[] array = new byte[$"url={url}".Length];
                Encoding.UTF8.GetBytes($"url={url}").CopyTo(array, 0);
                hwr.ContentLength = array.Length;

                using (var stream = hwr.GetRequestStream())
                {
                    stream.Write(array, 0, array.Length);
                    stream.Flush();
                }

                string link = String.Empty;
                using (HttpWebResponse response = (HttpWebResponse)hwr.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (MemoryStream sr = new MemoryStream())
                        {
                            stream?.CopyTo(sr);
                            byte[] result = sr.ToArray();
                            return result;
                        }
                    }
                }
            }
            catch (WebException e)
            {
                HttpWebRequest hwr = null;
                hwr = WebRequest.CreateHttp(e.Response.ResponseUri.AbsoluteUri);
                hwr.Proxy = WebRequest.DefaultWebProxy;
                hwr.UserAgent = "Other";
                hwr.Method = WebRequestMethods.Http.Get;
                using (var response = hwr.GetResponse())
                {
                    using (MemoryStream sr = new MemoryStream())
                    {
                        response.GetResponseStream().CopyTo(sr);
                        byte[] result = sr.ToArray();
                        return result;
                    }
                }
            }
        }

        private static string CorrectLink(string link)
        {
            return link.Replace("%", "%25")
                       .Replace(":", "%3A")
                       .Replace(";", "%3B")
                       .Replace("@", "%40")
                       .Replace("!", "%21")
                       .Replace("#", "%23")
                       .Replace("&", "%26")
                       .Replace("'", "%27")
                       .Replace(" ", "%20")
                       .Replace("?", "%3F")
                       .Replace("/", "%2F")
                       .Replace("=", "%3D")
                       .Replace("+", "%2B");
        }
    }
}
