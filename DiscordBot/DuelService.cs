﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiscordBot
{
    internal class DuelService
    {
        private static readonly Random rng = new Random();

        private DuelService()
        {
            Task.Factory.StartNew(InnerProcessingTask, TaskCreationOptions.LongRunning);
        }

        private static DuelService _instance;
        public static DuelService Instance => _instance ??= new DuelService();

        private async Task InnerProcessingTask()
        {
            while (true)
            {
                await Task.Delay(60 * 1000);
                var duels = Duels.Where(a => a.Value.dueDate <= DateTime.Now).ToList();
                List<(string winner, ISocketMessageChannel channel)> results = new();
                foreach (var duel in duels)
                {
                    var winner = GetWinner(duel.Key);
                    Duels.Remove(duel.Key);
                    results.Add((winner, duel.Value.channel));
                }
                foreach (var result in results)
                {
                    await result.channel.SendMessageAsync($":gun: Час расплаты :gun:");
                    await Task.Delay(1000);
                    await result.channel.SendMessageAsync(result.winner);
                }
            }
        }

        public async Task ProcessDuel(SocketCommandContext context)
        {
            if (Duels.TryGetValue(context.Channel.Id, out var duelValue))
            {
                var duel = duelValue.participants;
                if (duel.TryGetValue(context.User.Id, out bool participated))
                {
                    if (participated)
                    {
                        await context.Channel.SendMessageAsync($"Ты уже на месте. Не нужно повторяться.");
                        return;
                    }
                    duel[context.User.Id] = true;
                }
                else
                {
                    await context.Channel.SendMessageAsync($"Не лезь не в своё дело, {context.User.Mention}.");
                    return;
                }

                if (duel.Values.All(a => a))
                {
                    var winner = GetWinner(context.Channel.Id);
                    Duels.Remove(context.Channel.Id);
                    await context.Channel.SendMessageAsync($":gun: Час расплаты :gun:");
                    await Task.Delay(1000);
                    await context.Channel.SendMessageAsync(winner);
                }
            }
            else
            {
                if (context.Message.MentionedUsers.Count == 0)
                {
                    await context.Channel.SendMessageAsync("Решил покрасоваться?");
                    return;
                }
                if (context.Message.MentionedUsers.Any(a => a.Id == context.User.Id))
                {
                    await context.Channel.SendMessageAsync("Самоубийство - не выход.");
                    return;
                }
                Duels[context.Channel.Id] = (DateTime.Now.AddMinutes(5), context.Channel, context.Message.MentionedUsers.Select(a => (a.Id, false)).Union(new[] { (context.User.Id, true) }).ToDictionary(a => a.Id, a => a.Item2));
                await context.Channel.SendMessageAsync($"5 минут до полудня, господа. Пишите !duel, если не боитесь смерти.");
                var channelId = context.Channel.Id;
            }
        }

        private string GetWinner(ulong channel)
        {
            var participants = Duels[channel].participants.Where(a => a.Value).ToList();
            if (participants.Count == 1)
            {
                return $"Все остальные струсили, поэтому победитель - {MentionUtils.MentionUser(participants[0].Key)}";
            }
            else
            {
                var winner = participants[rng.Next(participants.Count)];
                var mention = MentionUtils.MentionUser(winner.Key);
                if (participants.Count > 2)
                {
                    return $"Последний оставшийся в живых - {mention}";
                }
                else
                {
                    return $"В перестрелке победил {mention}";
                }
            }
        }

        private Dictionary<ulong, (DateTime dueDate, ISocketMessageChannel channel, Dictionary<ulong, bool> participants)> Duels { get; set; } = new();
    }
}
