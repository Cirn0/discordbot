﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot
{
    public static class DateTimeHelper
    {
        // на удалённой машине московский часовой пояс, а дату нужно использовать новосибирскую; поэтому костылим вот так
        public static DateTime ShiftToNskTimeZone(this DateTime dateTime) => dateTime.AddHours(4);
    }
}
