﻿using Discord.Interactions;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DiscordBotService.Service;

namespace DiscordBotService.TypeConverters
{
    public class ReminderTimeSpanTypeConverter : TypeConverter
    {
        public override bool CanConvertTo(Type type) => type == typeof(ReminderTimeSpan);

        public override ApplicationCommandOptionType GetDiscordType() => ApplicationCommandOptionType.String;

        public override Task<TypeConverterResult> ReadAsync(IInteractionContext context, IApplicationCommandInteractionDataOption option, IServiceProvider services)
        {
            try
            {
                string input = option.Value?.ToString();
                var ts = ParseTimeString(input);
                if (ts == TimeSpan.Zero)
                {
                    return Task.FromResult(TypeConverterResult.FromError(InteractionCommandError.ConvertFailed, "Не удалось выяснить, через сколько нужно напомнить"));
                }
                return Task.FromResult(TypeConverterResult.FromSuccess(new ReminderTimeSpan { Time = ts }));
            }
            catch
            {
                return Task.FromResult(TypeConverterResult.FromError(InteractionCommandError.ConvertFailed, "Не удалось выяснить, через сколько нужно напомнить"));
            }
        }

        private static TimeSpan ParseTimeString(string str)
        {
            string hours = Regex.Match(str, @"\d+[hч]").Value;
            if (hours.Length > 0)
            {
                hours = hours[0..^1];
            }
            else
            {
                hours = "0";
            }
            string minutes = Regex.Match(str, @"\d+[mм]").Value;
            if (minutes.Length > 0)
            {
                minutes = minutes[0..^1];
            }
            else
            {
                minutes = "0";
            }

            return new TimeSpan(Int32.Parse(hours), Int32.Parse(minutes), 0);
        }
    }
}
