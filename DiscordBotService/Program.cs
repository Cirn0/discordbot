﻿using Discord;
using Discord.Commands;
using Discord.Interactions;
using Discord.WebSocket;
using DiscordBotService.Module;
using DiscordBotService.Service;
using DiscordBotService.TypeConverters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Reflection;

namespace DiscordBotService
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
            IHost host = Host.CreateDefaultBuilder(args)
                .ConfigureServices(services =>
                {
                    services.AddSingleton<DiscordSocketClient>(
                        new DiscordSocketClient(
                            new DiscordSocketConfig
                            {
                                AlwaysDownloadUsers = true,
                                GatewayIntents = GatewayIntents.AllUnprivileged | GatewayIntents.MessageContent | GatewayIntents.GuildMembers | GatewayIntents.GuildVoiceStates
                            }));
                    services.AddSingleton<InteractionService>(provider =>
                    {
                        var srv = new InteractionService(provider.GetRequiredService<DiscordSocketClient>(), new InteractionServiceConfig
                        {
                            DefaultRunMode = Discord.Interactions.RunMode.Sync
                        });
                        srv.AddTypeConverter<ReminderTimeSpan>(new ReminderTimeSpanTypeConverter());
                        return srv;
                    });
                    services.AddSingleton<CommandService>(provider => new CommandService(new CommandServiceConfig
                    {
                        CaseSensitiveCommands = false,
                        DefaultRunMode = Discord.Commands.RunMode.Async,
                        IgnoreExtraArgs = false,
                        SeparatorChar = ' '
                    }));
                    services.AddSingleton<SQLiteService>();
                    services.AddSingleton<BindService>();
                    services.AddSingleton<ReminderService>();
                    services.AddSingleton<RateService>();
                    services.AddSingleton<DuelService>();
                    services.AddSingleton<ImageService>();

                    services.AddHostedService<InteractionHandlingService>();
                    services.AddHostedService<CommandHandlingService>();
                    services.AddHostedService<StartupService>();
                })
                .UseSystemd()
                .Build();

            await host.RunAsync();
        }
    }
}

/*
 * 
 * Инструкция для установки приложения:
 * 1. Установить opus-tools
 * 2. Установить libvorbis-dev
 * 3. Установить (скопировать) ffmpeg в папку /usr/local/bin с именем ffmper-ytdl
 * 4. Установить (скопировать) yt-dlp в папку /usr/local/bin с именем youtube-dl
 * 5. В базе данных приложения (DiscordBot.db) в таблице Settings создать строку с именем token, в значении которой нужно указать токен бота
 * 6. Зарегистрировать и запустить systemd сервис в системе:
 *      - файл DiscordBot.service в каталог /etc/systemd/system
 *      - sudo systemctl daemon-reload
 *      - sudo systemctl enable DiscordBot
 *      - sudo systemctl start DiscordBot
 * 
 */