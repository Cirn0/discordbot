﻿using Discord.Interactions;
using DiscordBotService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Module
{
    public class LockingModule : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly SQLiteService sqliteService;

        public LockingModule(SQLiteService sqliteService)
        {
            this.sqliteService = sqliteService;
        }

        //[SlashCommand("sleep", $"Бот перестаёт отвечать на команды")]
        [RequireOwner]
        public async Task SleepCommand()
        {
            sqliteService.SetSettings("locked", "1");
            await RespondAsync("Ушла спать. На запросы не отвечаю. Просьба не будить.");
        }

        //[SlashCommand("wakeup", $"Бот снова начинает отвечать на команды")]
        [RequireOwner]
        public async Task WakeUpCommand()
        {
            sqliteService.SetSettings("locked", "0");
            await RespondAsync("Здравствуйте. Можете задавать свои вопросы.");
        }
    }
}
