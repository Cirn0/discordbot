﻿using Discord.Interactions;
using Discord.WebSocket;
using DiscordBotService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Module
{
    public class DuelModule : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly DuelService duelService;

        public DuelModule(DuelService duelService)
        {
            this.duelService = duelService;
        }

        [SlashCommand("duel", "Провести дуэль с указанными людьми")]
        public async Task DuelCommand(SocketUser first = null, SocketUser second = null, SocketUser third = null, SocketUser fourth = null, SocketUser fifth = null)
        {
            var result = await duelService.ProcessDuel(Context, new[] { first, second, third, fourth, fifth }.Where(a => a != null).ToList());
            await RespondAsync(result.answer);
            await result.additionalTask;
        }
    }
}
