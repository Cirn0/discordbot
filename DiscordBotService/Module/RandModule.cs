﻿using Discord.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Module
{
    public class RandModule : InteractionModuleBase<SocketInteractionContext>
    {
        private static Random rng = new();

        [SlashCommand("rand", $"Вернуть случайное целое число от {nameof(first)} до {nameof(second)} (границы включаются)")]
        public Task RandomCommand(
            [Summary(description: "Нижняя граница")] int first,
            [Summary(description: "Верхняя граница")] int second) =>
            RespondAsync($"Ну, допустим, {rng.Next(Math.Min(first, second), Math.Max(first, second) + 1)}.");
    }
}
