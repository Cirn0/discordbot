﻿using Discord.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Module
{
    public class BruhModule : InteractionModuleBase<SocketInteractionContext>
    {
        [SlashCommand("bruh", "bruh")]
        public Task BruhCommand() =>
            RespondWithFileAsync("bruh.jpg");
    }
}
