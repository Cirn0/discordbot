﻿using Discord.Interactions;
using DiscordBotService.Service;

namespace DiscordBotService.Module
{
    public class RemindModule : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly ReminderService reminderService;

        public RemindModule(ReminderService reminderService)
        {
            this.reminderService = reminderService;
        }

        [SlashCommand("remind", $"Напомнить о чём-либо через указанное время")]
        public async Task RemindCommand(
            [Summary(description: "Время, через которое надо напомнить, в формате <H>h<M>m")] ReminderTimeSpan time,
            string message)
        {
            reminderService.Remind(Context, DateTime.Now + time.Time, message);
            await RespondAsync($"Хорошо. Я вам напомню через {FormatTimeString(time.Time)}");
        }

        private static string FormatTimeString(TimeSpan ts)
        {
            string s = String.Empty;
            int h = (int)ts.TotalHours;
            if (h != 0)
            {
                if (h % 100 >= 10 && h % 100 <= 20)
                {
                    s += $"{h} часов ";
                }
                else if (h % 10 == 1)
                {
                    s += $"{h} час ";
                }
                else if (h % 10 >= 2 && h % 10 <= 4)
                {
                    s += $"{h} часа ";
                }
                else
                {
                    s += $"{h} часов ";
                }
            }
            int m = ts.Minutes;
            if (m != 0)
            {
                if (m % 100 >= 10 && m % 100 <= 20)
                {
                    s += $"{m} минут";
                }
                else if (m % 10 == 1)
                {
                    s += $"{m} минуту";
                }
                else if (m % 10 >= 2 && m % 10 <= 4)
                {
                    s += $"{m} минуты";
                }
                else
                {
                    s += $"{m} минут";
                }
            }

            return s.Trim();
        }
    }
}
