﻿using Discord;
using Discord.Interactions;
using DiscordBotService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Module
{
    public class ImageModule : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly ImageService imageService;

        public ImageModule(ImageService imageService)
        {
            this.imageService = imageService;
        }

        [SlashCommand("hi", "Поздороваться")]
        public async Task HiCommand()
        {
            await InnerGetImage("makise_kurisu solo", Boorus.Safebooru, "Привет!");
        }

        [SlashCommand("image", "Поиск изображения на TBIB")]
        public async Task ImageCommand([Summary(description: "Теги для поиска")] string tags)
        {
            await InnerGetImage(tags, Boorus.TBIB);
        }

        [SlashCommand("safeimage", "Поиск изображения на Safebooru")]
        public async Task SafeImageCommand([Summary(description: "Теги для поиска")] string tags)
        {
            await InnerGetImage(tags, Boorus.Safebooru);
        }


        private async Task InnerGetImage(string tags, Boorus booru, string message = null)
        {
            await DeferAsync();
            var result = await imageService.GetImage(tags, booru, message);

            await ModifyOriginalResponseAsync(props =>
            {
                props.Content = result.Message;
                if (!String.IsNullOrEmpty(result.FilePath))
                {
                    props.Attachments = new List<FileAttachment>()
                    {
                        new FileAttachment(result.FilePath)
                    };
                }
            });

            try
            {
                File.Delete(result.FilePath);
            }
            catch { }
        }
    }
}
