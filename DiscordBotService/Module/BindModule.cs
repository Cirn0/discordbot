﻿using Discord;
using Discord.Commands;
using Discord.Interactions;
using DiscordBotService.Service;

namespace DiscordBotService.Module
{
    public class BindModule : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly SQLiteService sqliteService;
        private readonly BindService bindService;

        public BindModule(SQLiteService sqliteService, BindService bindService)
        {
            this.sqliteService = sqliteService;
            this.bindService = bindService;
        }

        [SlashCommand("bind", $"Если пользователь запостит сообщение, состоящее из !{nameof(command)}, бот удалит его и запостит {nameof(value)}")]
        public async Task BindCommand(string command, string value)
        {
            sqliteService.AddBind(Context.Channel.Id, Context.User.Id, command, value);
            await bindService.UpdateBinds();
        }
    }
}
