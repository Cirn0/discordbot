﻿using Discord;
using Discord.Interactions;
using Discord.Rest;
using Discord.WebSocket;
using DiscordBotService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Module
{
    public class GameModule : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly SQLiteService sqliteService;

        public GameModule(SQLiteService sqliteService)
        {
            this.sqliteService = sqliteService;
        }

        private static bool currentlyInGame = false;
        private readonly Random rng = new();
        [SlashCommand("game", "Сыграть в игру")]
        public async Task GameCommand(GameMode mode = GameMode.None)
        {
            if (currentlyInGame)
            {
                return;
            }
            try
            {
                currentlyInGame = true;
                ulong channelId = Context.Channel.Id;
                if (mode == GameMode.Stats)
                {
                    var stats = sqliteService.GetWinnerStats(channelId);
                    StringBuilder sb = new StringBuilder();
                    for (int place = 0; place < stats.Count; ++place)
                    {
                        if (place == 0)
                        {
                            var tenryu = (Context.Channel as SocketTextChannel).Guild.Emotes.SingleOrDefault(a => a.Name == "tenryu");
                            var rmuscle = (Context.Channel as SocketTextChannel).Guild.Emotes.SingleOrDefault(a => a.Name == "rmuscle");
                            sb.AppendLine($"Текущий лидер: {stats[place].Item1} с {stats[place].Item2} победами. {(tenryu == null || rmuscle == null ? "" : $":muscle: {tenryu} {rmuscle}")}");
                        }
                        else
                        {
                            sb.AppendLine($"{place + 1} место: {stats[place].Item1} с {stats[place].Item2} победами.");
                        }
                    }
                    await RespondAsync(sb.ToString());
                    return;
                }
                Winner winner = sqliteService.GetWinner(channelId);

                if (winner != null)
                {
                    await RespondAsync($"На данный момент победитель - {winner.User}. До переигровки осталось {winner.RemainingTime} минут.");
                    return;
                }

                var oldStats = sqliteService.GetWinnerStats(channelId);

                var users = (Context.Channel as SocketGuildChannel).Guild.Users.Where(a => !a.IsBot).ToList();
                int random = rng.Next(users.Count());
                string nick = users.ToArray()[random].Username;
                sqliteService.SetWinner(nick, channelId);
                var currentStats = sqliteService.GetWinnerStats(channelId);

                int prevIndex = oldStats.Select(a => a.Item1).ToList().IndexOf(nick);
                int currIndex = currentStats.Select(a => a.Item1).ToList().IndexOf(nick);
                string mainMessage = $"Победитель: {nick}";
                if (currIndex != prevIndex)
                {
                    await RespondAsync($"{mainMessage}! Благодаря этому он поднимается на {currIndex + 1} место в общем рейтинге. Поздравляем!");
                }
                else
                {
                    await RespondAsync($"{mainMessage}! Поздравим его!");
                }

                var first = currentStats.First();
                if (first.Item2 >= 50)
                {
                    await Context.Channel.SendFileAsync(@"hug.jpg", $"И у нас есть безоговорочный победитель: {first.Item1}! Поздравим его!");
                    sqliteService.SetSettings($"WinnerBeginDate_{channelId}", DateTime.Now.ToString("yyyyMMddHHmmss"));
                }
                return;
            }
            finally
            {
                currentlyInGame = false;
            }
        }
    }

    public enum GameMode
    {
        [Hide]
        None,
        Stats
    }
}
