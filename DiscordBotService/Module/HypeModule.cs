﻿using Discord.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Module
{
    public class HypeModule : InteractionModuleBase<SocketInteractionContext>
    {
        [SlashCommand("hype", "Немножечко хайпануть")]
        public Task HypeCommand() => RespondAsync("Люблю хайп:fire::fire: и все что с ним связано:stuck_out_tongue_winking_eye::stuck_out_tongue_winking_eye:, музыка элджей :two_hearts::two_hearts::sunglasses::ok_hand:лил пип:heart_eyes::heart_eyes::astonished: пошлая молли :smiling_imp::smiling_imp: гспд рейв эпидемия) :laughing::laughing:\r\nШмот вэнсы :heartpulse::heartpulse: толстовка трешер :shirt::bow:\r\nОбычно я хайплю :scream_cat::scream_cat:, это мое хобби :joy_cat::joy_cat:");
    }
}
