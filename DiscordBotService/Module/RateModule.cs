﻿using Discord.Interactions;
using DiscordBotService.Service;

namespace DiscordBotService.Module
{
    public class RateModule : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly RateService rateService;

        public RateModule(RateService rateService)
        {
            this.rateService = rateService;
        }

        [SlashCommand("rate", "Оценить по десятибалльной шкале")]
        public Task RateCommand([Summary(description: "Предмет для оценки")] string subject)
        {
            return RespondAsync($"Я оцениваю {subject} на {rateService.RateString(subject)} из 10.");
        }
    }
}
