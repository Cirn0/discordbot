﻿using Discord.Interactions;
using DiscordBotService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Module
{
    public class ChooseModule : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly RateService rateService;

        public ChooseModule(RateService rateService)
        {
            this.rateService = rateService;
        }

        [SlashCommand("choose", "Выбрать из нескольких вариантов")]
        public Task ChooseCommand(string first, string second, string third = null, string fourth = null, string fifth = null)
        {
            string[] variants = new[] { first, second, third, fourth, fifth }.Where(a => !String.IsNullOrEmpty(a)).ToArray();
            if (variants.Length < 2)
            {
                return RespondAsync("Маловато вариантов для выбора");
            }

            string result = variants.Select(a => a.Trim())
                                    .Select(a => (Source: a, Rate: rateService.RateString(a)))
                                    .Select(a => (Source: a.Source, Rate: Int32.TryParse(a.Rate, out int result) ? result : 11))
                                    .OrderByDescending(a => a.Rate)
                                    .ThenBy(a => a.Source)
                                    .First()
                                    .Source;

            return RespondAsync($"Победил вариант \"{result}\"");
        }
    }
}
