﻿using Discord.Audio;
using Discord.Interactions;
using Discord.WebSocket;
using System.Diagnostics;

namespace DiscordBotService.Module
{
    public class VoiceModule : InteractionModuleBase<SocketInteractionContext>
    {
        private static readonly Dictionary<ulong, CancellationTokenSource> playingCancellationTokens = new Dictionary<ulong, CancellationTokenSource>();
        private const int BUFFER_SIZE = 2500;

        [SlashCommand("play", "Воспроизводит видео с youtube", runMode: RunMode.Async)]
        public async Task PlayCommand([Summary(description: "Часть ссылки на youtube-видео после v, например dQw4w9WgXcQ")] string link)
        {
            var voiceChannel = Context.Guild.VoiceChannels.FirstOrDefault(a => a.ConnectedUsers.Any(a => a.Id == Context.User.Id));
            ulong channelId = Context.Channel.Id;

            if (playingCancellationTokens.ContainsKey(channelId))
            {
                await RespondAsync($"Извините, сейчас не могу.");
                return;
            }

            await DeferAsync();

            Process p = null;
            Process p1 = null;

            var token = new CancellationTokenSource();
            playingCancellationTokens.Add(channelId, token);
            token.Token.Register(() =>
            {
                if (p != null)
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                    try
                    {
                        p.Close();
                    }
                    catch { }
                }
                if (p1 != null)
                {
                    try
                    {
                        p1.Kill();
                    }
                    catch { }
                    try
                    {
                        p1.Close();
                    }
                    catch { }
                }
            });

            string audioFile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + ".wav");

            string videoFile = $"{link}.mp4";
            foreach (var ch in Path.GetInvalidFileNameChars())
            {
                videoFile = videoFile.Replace(ch, '_');
            }
            var dir = Path.Combine("DiscordBot", "yt");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            videoFile = Path.Combine(dir, videoFile);

            IAudioClient client = null;

            const string FILENAME_FILE_EXTENSION = ".txt";
            string filenameFile = videoFile + FILENAME_FILE_EXTENSION;

            try
            {
                if (!File.Exists(videoFile))
                {
                    ProcessStartInfo psi1 = new ProcessStartInfo
                    {
                        FileName = "youtube-dl",
                        Arguments = $"-i -o {videoFile} {link} --geo-bypass -q --print-to-file \"%(title)s\" {filenameFile}",
                    };
                    p1 = Process.Start(psi1);
                    p1.WaitForExit();
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }
                }

                // yt-dlp при скачивании файла не в формате mp4 допишет к нему реальное расширение, то есть например asdfg.mp4.webm.
                // Поэтому после скачивания дополнительно ищем подходящий файл в каталоге.
                videoFile = Directory
                    .EnumerateFiles(dir)
                    .First(a => Path.GetFileName(a).StartsWith(Path.GetFileName(videoFile)) && 
                               !String.Equals(Path.GetExtension(a), FILENAME_FILE_EXTENSION, StringComparison.OrdinalIgnoreCase));

                ProcessStartInfo psi = new ProcessStartInfo
                {
                    FileName = "ffmpeg-ytdl",
                    Arguments = $"-xerror -y -i \"{videoFile}\" -ac 2 -f s16le -vn -ar 48000 pipe:1",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                };
                p = Process.Start(psi);

                SocketVoiceChannel vc = voiceChannel ?? (Context.Channel as SocketGuildChannel).Guild.VoiceChannels.First();
                client = await vc.ConnectAsync();
                using var audioStream = p.StandardOutput.BaseStream;
                using AudioOutStream discord = client.CreatePCMStream(AudioApplication.Mixed);
                string videoName = File.ReadAllText(filenameFile);
                await ModifyOriginalResponseAsync(props =>
                {
                    props.Content = $"Воспроизводится видео \"{videoName.Trim()}\"";
                });
                try
                {
                    await audioStream.CopyToAsync(discord, token.Token);
                }
                finally
                {
                    await discord.FlushAsync();
                }
            }
            catch (OperationCanceledException)
            {
                return;
            }
            catch (Exception)
            {
                await ModifyOriginalResponseAsync(props =>
                {
                    props.Content = $"Ошибка воспроизведения видео";
                });
                return;
            }
            finally
            {
                playingCancellationTokens.Remove(channelId);
                File.Delete(audioFile);
                File.Delete(filenameFile);
                if (client != null)
                {
                    await client.StopAsync();
                }
            }
            return;
        }

        [SlashCommand("stop", "Останавливает текущее воспроизводимое видео")]
        public async Task StopCommand()
        {
            ulong channelId = Context.Channel.Id;
            if (playingCancellationTokens.TryGetValue(channelId, out var value))
            {
                value.Cancel();
                playingCancellationTokens.Remove(channelId);
                await RespondAsync("Воспроизведение остановлено");
            }
            else
            {
                await RespondAsync("Нечего останавливать");
            }
        }
    }
}
