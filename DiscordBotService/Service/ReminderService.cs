﻿using Discord;
using Discord.Commands;
using Discord.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DiscordBotService.Service
{
    public class ReminderService
    {
        public ReminderService()
        {
            Task.Factory.StartNew(InnerProcessingTask, TaskCreationOptions.LongRunning);
        }

        private readonly List<(SocketInteractionContext context, DateTime when, string message)> reminders = new();

        public void Remind(SocketInteractionContext context, DateTime when, string message)
        {
            reminders.Add((context, when, message));
            cts.Cancel();
        }

        CancellationTokenSource cts = new CancellationTokenSource();
        private async Task InnerProcessingTask()
        {
            while (true)
            {
                cts = new CancellationTokenSource();
                var val = reminders.OrderBy(a => a.when).FirstOrDefault();
                try
                {
                    if (val == default || val.when < DateTime.Now)
                    {
                        await Task.Delay(-1, cts.Token);
                    }
                    else
                    {
                        await Task.Delay(val.when - DateTime.Now, cts.Token);
                    }
                }
                catch (OperationCanceledException)
                {
                    continue;
                }

                await val.context.Channel.SendMessageAsync($"{val.context.User.Mention}, вы просили напомнить: \r\n{val.message}");
                reminders.Remove(val);
            }
        }
    }

    public class ReminderTimeSpan
    {
        public TimeSpan Time { get; init; }
    }
}
