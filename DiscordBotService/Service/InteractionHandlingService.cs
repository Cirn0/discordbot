﻿using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using DiscordBotService.Service.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace DiscordBotService.Service
{
    internal class InteractionHandlingService : IHostedService
    {
        private readonly DiscordSocketClient discord;
        private readonly InteractionService interactions;
        private readonly IServiceProvider services;
        private readonly ILogger<InteractionHandlingService> logger;

        public InteractionHandlingService(
            DiscordSocketClient discord,
            InteractionService interactions,
            IServiceProvider services,
            ILogger<InteractionHandlingService> logger)
        {
            this.discord = discord;
            this.interactions = interactions;
            this.services = services;
            this.logger = logger;

            interactions.Log += message => LogHelper.Log(logger, message);
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            discord.Ready += () => interactions.RegisterCommandsGloballyAsync();
            discord.InteractionCreated += OnInteractionAsync;

            var modules = await interactions.AddModulesAsync(Assembly.GetEntryAssembly(), services);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            interactions.Dispose();
            return Task.CompletedTask;
        }

        private async Task OnInteractionAsync(SocketInteraction interaction)
        {
            try
            {
                var context = new SocketInteractionContext(discord, interaction);
                var result = await interactions.ExecuteCommandAsync(context, services);

                if (!result.IsSuccess)
                {
                    await interaction.RespondAsync(result.ErrorReason);
                }
            }
            catch
            {
                if (interaction.Type == InteractionType.ApplicationCommand)
                {
                    await interaction.GetOriginalResponseAsync()
                        .ContinueWith(msg => msg.Result.DeleteAsync());
                }
            }
        }
    }
}
