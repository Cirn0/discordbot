﻿namespace DiscordBotService.Service
{
    public struct Image
    {
        public string Path { get; set; }
        public string Tags { get; set; }
        public string Rating { get; set; }
    }

    public class Rooster
    {
        public string User { get; set; }
        public int RemainingTime { get; set; }
    }

    public class Winner
    {
        public string User { get; set; }
        public int RemainingTime { get; set; }
    }

    public enum Boorus
    {
        Safebooru,
        TBIB,
        Derpibooru
    }

    public struct BindKey
    {
        public ulong ChannelId { get; set; }
        public ulong UserId { get; set; }
        public string Bind { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is BindKey key))
            {
                return false;
            }
            return this.ChannelId == key.ChannelId &&
                   this.UserId == key.UserId &&
                   this.Bind == key.Bind;
        }
    }
}
