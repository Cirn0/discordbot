﻿using Discord.Commands;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotService.Service.Helpers;

namespace DiscordBotService.Service
{
    public class ImageService
    {
        private readonly SQLiteService sqliteService;
        public ImageService(SQLiteService sqliteService)
        {
            this.sqliteService = sqliteService;
        }

        private static readonly Dictionary<Boorus, IBooruApiHelper> apiHelpers = new Dictionary<Boorus, IBooruApiHelper>
        {
            { Boorus.Safebooru, new SafebooruApiHelper() },
            { Boorus.TBIB, new TBIBApiHelper() },
            { Boorus.Derpibooru, new DerpibooruApiHelper() }
        };
        private static readonly string[] prohibitedUnsafeTags = { "makise_kurisu" };
        public async Task<ImageResult> GetImage(string tags, Boorus booru, string message = null)
        {
            if (prohibitedUnsafeTags.Any(a => tags.Contains(a)) && booru != Boorus.Safebooru && !tags.Contains("rating:safe"))
            {
                return new ImageResult(null, "Ты это что такое ищешь? Этот тег можно искать только с \"rating:safe\".", false);
            }
            Image? img = sqliteService.GetImage(tags, booru);
            while (img == null)
            {
                List<Image> images = GetImages(tags, booru);
                if (images.Count == 0)
                {
                    return new ImageResult(null, "Не найдено ни одного изображения по тегу", false);
                }
                sqliteService.AddImages(tags, images, booru);
                img = sqliteService.GetImage(tags, booru);
            }

            string href = img.Value.Path;
            Console.WriteLine(href);
            byte[] image = HttpRequestHelper.GetResponse(href);

            string tempFile = Path.Combine(Path.GetTempPath(), Path.GetFileName(href));
            File.WriteAllBytes(tempFile, image);
            return new ImageResult(tempFile, message ?? $"{tags}\r\n\r\n{img.Value.Tags}", !String.Equals(img.Value.Rating, "safe", StringComparison.OrdinalIgnoreCase));
        }

        private static readonly Dictionary<(string, Boorus), int> loadedPages = new Dictionary<(string, Boorus), int>();
        private static List<Image> GetImages(string tag, Boorus booru)
        {
            List<Image> imagesList = new List<Image>();
            int page = 0;
            var key = (tag, booru);
            if (loadedPages.ContainsKey(key))
            {
                page = ++loadedPages[key];
                loadedPages.Remove(key);
            }
            loadedPages.Add(key, page);

            List<Image> images = apiHelpers[booru].GetImages(tag, page);

            return images.Where(a => a.Rating.Equals("safe") || !prohibitedUnsafeTags.Any(b => a.Tags.Contains(b))).ToList();
        }
    }

    public record ImageResult(string FilePath, string Message, bool IsNsfw);
}
