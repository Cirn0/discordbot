﻿using Discord;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Service.Helpers
{
    internal static class LogHelper
    {
        public static Task Log(ILogger logger, LogMessage message)
        {
            LogLevel level = message.Severity switch
            {
                LogSeverity.Critical => LogLevel.Critical,
                LogSeverity.Error => LogLevel.Error,
                LogSeverity.Warning => LogLevel.Warning,
                LogSeverity.Info => LogLevel.Information,
                LogSeverity.Verbose => LogLevel.Trace,
                LogSeverity.Debug => LogLevel.Debug,
                _ => LogLevel.Warning
            };
            logger.Log(level, message.ToString());

            return Task.CompletedTask;
        }
    }
}
