﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Service.Helpers
{
    public interface IBooruApiHelper
    {
        List<Image> GetImages(string tag, int page);
    }

    public class SafebooruApiHelper : IBooruApiHelper
    {
        public List<Image> GetImages(string tag, int page)
        {
            List<Image> imagesList = new List<Image>();
            string reqAddr = $@"http://safebooru.org/index.php?page=dapi&s=post&q=index&tags={tag.Replace(" ", "%20")}&pid={page}&json=1&sort:rating:desc";
            string imageAddrFormat = @"http://safebooru.org/images/{0}/{1}";

            string json = Encoding.UTF8.GetString(HttpRequestHelper.GetResponse(reqAddr));

            if (String.IsNullOrEmpty(json))
            {
                return new List<Image>();
            }
            var obj = JArray.Parse(json);
            foreach (var v in obj)
            {
                string dirName = (v["directory"] as JValue).Value.ToString();
                string imageName = (v["image"] as JValue).Value.ToString();
                string tags = (v["tags"] as JValue).Value.ToString();
                string rating = (v["rating"] as JValue).Value.ToString();
                string href = String.Format(imageAddrFormat, dirName, imageName);

                Image img = new Image { Path = href, Tags = tags, Rating = rating };
                imagesList.Add(img);
            }

            return imagesList;
        }
    }

    public class TBIBApiHelper : IBooruApiHelper
    {
        public List<Image> GetImages(string tag, int page)
        {
            List<Image> imagesList = new List<Image>();
            string reqAddr = $@"http://tbib.org/index.php?page=dapi&s=post&q=index&tags={tag.Replace(" ", "%20")}&pid={page}&json=1&sort:rating:desc";
            string imageAddrFormat = @"http://tbib.org/images/{0}/{1}";

            string json = Encoding.UTF8.GetString(HttpRequestHelper.GetResponse(reqAddr));

            if (String.IsNullOrEmpty(json))
            {
                return new List<Image>();
            }
            var obj = JArray.Parse(json);
            foreach (var v in obj)
            {
                string dirName = (v["directory"] as JValue).Value.ToString();
                string imageName = (v["image"] as JValue).Value.ToString();
                string tags = (v["tags"] as JValue).Value.ToString();
                string rating = (v["rating"] as JValue).Value.ToString();
                string href = String.Format(imageAddrFormat, dirName, imageName);

                Image img = new Image { Path = href, Tags = tags, Rating = rating };
                imagesList.Add(img);
            }

            return imagesList;
        }
    }

    public class DerpibooruApiHelper : IBooruApiHelper
    {
        public List<Image> GetImages(string tag, int page)
        {
            List<Image> imagesList = new List<Image>();
            string reqAddr = $@"https://derpibooru.org/api/v1/json/search/images?q={tag.Replace(' ', '+')}&page={++page}";        //страницы тут начинаются с 1

            string json = Encoding.UTF8.GetString(HttpRequestHelper.GetResponse(reqAddr));

            if (String.IsNullOrEmpty(json))
            {
                return new List<Image>();
            }
            var obj = JObject.Parse(json);
            foreach (var v in (obj["images"] as JArray))
            {
                string tags = String.Join(" ", (v["tags"] as JArray).OfType<JValue>().Select(a => a.Value?.ToString()));
                string href = (v["representations"]["full"] as JValue).Value.ToString();

                Image img = new Image { Path = href, Tags = tags, Rating = "questionable" };
                imagesList.Add(img);
            }

            return imagesList;
        }
    }
}
