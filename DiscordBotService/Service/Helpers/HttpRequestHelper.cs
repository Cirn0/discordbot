﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Service.Helpers
{
    public static class HttpRequestHelper
    {
        public static byte[] GetResponse(string url)
        {
            HttpWebRequest req = WebRequest.CreateHttp(url);
            req.Proxy = WebRequest.DefaultWebProxy;
            req.UserAgent = "Other";
            req.Method = WebRequestMethods.Http.Get;
            using (var resp = req.GetResponse())
            {
                using (MemoryStream sr = new MemoryStream())
                {
                    resp.GetResponseStream().CopyTo(sr);
                    return sr.ToArray();
                }
            }
        }
    }
}
