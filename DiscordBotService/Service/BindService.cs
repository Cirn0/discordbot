﻿using Discord.Commands;

namespace DiscordBotService.Service
{
    public class BindService
    {
        private readonly CommandService commandService;
        private readonly SQLiteService sqliteService;
        public BindService(CommandService commandService, SQLiteService sqliteService)
        {
            this.commandService = commandService;
            this.sqliteService = sqliteService;
        }

        private readonly List<Discord.Commands.ModuleInfo> registeredModules = new List<Discord.Commands.ModuleInfo>();

        public async Task UpdateBinds()
        {
            foreach (var module in registeredModules)
            {
                await commandService.RemoveModuleAsync(module);
            }
            registeredModules.Clear();

            foreach (var item in sqliteService.GetBinds().GroupBy(a => a.Key.Bind))
            {
                var mi = await commandService.CreateModuleAsync(item.Key, a =>
                {
                    var dic = item.ToDictionary(k => (k.Key.ChannelId, k.Key.UserId), v => v.Value);
                    a.AddCommand("", async (context, objs, serviceProvider, commandInfo) =>
                    {
                        if (dic.TryGetValue((context.Channel.Id, context.User.Id), out string value))
                        {
                            await context.Channel.SendMessageAsync(value, messageReference: context.Message.Reference);
                            try
                            {
                                await context.Message.DeleteAsync();
                            }
                            catch
                            {
                                // Нет прав на удаление чужих сообщений. Ничего не поделать
                            }
                        }
                    },
                    builder =>
                    {
                        builder.WithPriority(9);
                    }).Build(commandService, null);
                });
                registeredModules.Add(mi);
            }
        }
    }
}
