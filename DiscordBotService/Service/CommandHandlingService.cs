﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using DiscordBotService.Module;
using DiscordBotService.Service.Helpers;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace DiscordBotService.Service
{
    internal class CommandHandlingService : IHostedService
    {
        private readonly DiscordSocketClient discord;
        private readonly CommandService command;
        private readonly IServiceProvider services;
        private readonly BindService bindService;
        private readonly ILogger<CommandHandlingService> logger;

        public CommandHandlingService(
            DiscordSocketClient discord,
            CommandService command,
            IServiceProvider services,
            BindService bindService,
            ILogger<CommandHandlingService> logger)
        {
            this.discord = discord;
            this.command = command;
            this.services = services;
            this.bindService = bindService;
            this.logger = logger;

            command.Log += message => LogHelper.Log(logger, message);
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            discord.MessageReceived += MessageReceived;
            command.CommandExecuted += CommandExecuted;
            await bindService.UpdateBinds();
        }

        private async Task MessageReceived(SocketMessage arg)
        {
            if (arg is not SocketUserMessage message)
            {
                return;
            }
            int argPos = 0;
            if (message.HasCharPrefix('!', ref argPos))
            {
                var context = new SocketCommandContext(discord, message);
                await command.ExecuteAsync(context, argPos, services);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        private async Task CommandExecuted(Optional<CommandInfo> arg1, ICommandContext arg2, IResult arg3)
        {
            if (!arg3.IsSuccess && arg3.Error != CommandError.UnknownCommand)
            {
                await arg2.Channel.SendMessageAsync($"Некорректный вызов команды: {arg3.ErrorReason}");
            }
        }
    }
}
