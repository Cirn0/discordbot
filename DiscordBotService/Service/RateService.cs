﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Service
{
    public class RateService
    {
        private readonly SQLiteService sqliteService;
        private readonly Random rng = new();

        public RateService(SQLiteService sqliteService)
        {
            this.sqliteService = sqliteService;
        }

        public string RateString(string obj)
        {
            string rate = sqliteService.GetRate(obj);
            if (rate == null)
            {
                rate = rng.Next(1, 11).ToString();
                sqliteService.SetRate(obj, rate);
            }

            return rate;
        }
    }
}
