﻿using Discord;
using Discord.WebSocket;
using DiscordBotService.Service.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotService.Service
{
    internal class StartupService : IHostedService
    {
        private readonly SQLiteService sqliteService;
        private readonly DiscordSocketClient discord;
        private readonly ILogger<StartupService> logger;

        public StartupService(SQLiteService sqliteService, DiscordSocketClient discord, ILogger<StartupService> logger)
        {
            this.sqliteService = sqliteService;
            this.discord = discord;
            this.logger = logger;

            discord.Log += message => LogHelper.Log(logger, message);
            TaskScheduler.UnobservedTaskException += UnobservedTaskException;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await discord.LoginAsync(TokenType.Bot, sqliteService.GetSettings("token"));
            await discord.StartAsync();
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await discord.LogoutAsync();
            await discord.StopAsync();
        }

        private void UnobservedTaskException(object? sender, UnobservedTaskExceptionEventArgs e)
        {
            logger.LogCritical(e.Exception, "");
        }
    }
}
