﻿using DiscordBotService.Service.Helpers;
using System.Data.SQLite;

namespace DiscordBotService.Service
{
    public class SQLiteService
    {
        SQLiteConnection connection = new SQLiteConnection("Data Source = DiscordBot.db");

        public SQLiteService()
        {
            connection.Open();
        }

        public string GetSettings(string settingsName)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = $"select value from Settings where name = @name";
                comma.Parameters.Add(new SQLiteParameter("@name", settingsName));
                string res = (string)comma.ExecuteScalar();

                return res;
            }
        }

        public void SetSettings(string settingsName, string value)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = $"insert or replace into Settings (Name, Value) values (@name, @Value)";
                comma.Parameters.Add(new SQLiteParameter("@name", settingsName));
                comma.Parameters.Add(new SQLiteParameter("@value", value));
                comma.ExecuteNonQuery();
            }
        }

        public Image? GetImage(string tag, Boorus booru)
        {
            long ID;
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = $"select ID from ImageSearches where Tags = @tags and Booru = '{booru.ToString()}'";
                comma.Parameters.Add(new SQLiteParameter("@tags", tag));
                object innerID = comma.ExecuteScalar();
                if (innerID == null)
                {
                    return null;
                }
                ID = (long)innerID;
            }
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = $"select Path, Tags, Rating from Images where SearchID = {ID} and IsUsed = 0";
                var dataReader = comma.ExecuteReader();
                if (!dataReader.Read())
                {
                    return null;
                }
                string path = dataReader.GetString(0);
                string tags = dataReader.GetString(1);
                string rating = dataReader.GetString(2);

                using (SQLiteCommand updateComma = new SQLiteCommand(connection))
                {
                    updateComma.CommandText = "update Images set IsUsed = 1 where Path = @path";
                    updateComma.Parameters.Add(new SQLiteParameter("@path", path));
                    updateComma.ExecuteNonQuery();
                }

                return new Image { Path = path, Tags = tags, Rating = rating };
            }
        }

        public void AddImages(string tag, IEnumerable<Image> images, Boorus booru)
        {
            long ID;
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = $"select ID from ImageSearches where Tags = @tags and Booru = '{booru.ToString()}'";
                comma.Parameters.Add(new SQLiteParameter("@tags", tag));
                object innerID = comma.ExecuteScalar();
                if (innerID == null)
                {
                    using (SQLiteCommand comma2 = new SQLiteCommand(connection))
                    {
                        comma2.CommandText = $"insert into ImageSearches (Tags, Booru) values (@tags, '{booru.ToString()}')";
                        comma2.Parameters.Add(new SQLiteParameter("@tags", tag));
                        comma2.ExecuteNonQuery();
                    }
                    innerID = comma.ExecuteScalar();
                }
                ID = (long)innerID;
            }

            SQLiteTransaction tran = connection.BeginTransaction();
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.Transaction = tran;
                comma.CommandText = $"insert or ignore into Images (SearchId, Path, Tags, Rating) values ({ID}, @path, @tags, @rating)";
                foreach (var img in images)
                {
                    comma.Parameters.Clear();
                    comma.Parameters.Add(new SQLiteParameter("@path", img.Path));
                    comma.Parameters.Add(new SQLiteParameter("@tags", img.Tags));
                    comma.Parameters.Add(new SQLiteParameter("@rating", img.Rating));
                    comma.ExecuteNonQuery();
                }
                tran.Commit();
            }
            tran.Dispose();
        }

        public Winner GetWinner(ulong guildId)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = $"select UserName, ChoosingDate from Winner where Channel = @channel order by ChoosingDate desc limit 1";
                comma.Parameters.Add(new SQLiteParameter("@channel", guildId));
                SQLiteDataReader dr = comma.ExecuteReader();
                if (!dr.Read())
                {
                    return null;
                }
                string date = dr.GetString(1);
                DateTime lastDate = DateTime.ParseExact(date, "yyyyMMddHHmmss", null);
                if ((DateTime.Now - lastDate).TotalHours >= 1)
                {
                    return null;
                }

                return new Winner { User = dr.GetString(0), RemainingTime = 60 - (DateTime.Now - lastDate).Minutes };
            }
        }

        public List<(string, int)> GetWinnerStats(ulong guildId)
        {
            List<(string, int)> result = new List<(string, int)>();
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = $"select count(*), UserName from Winner where ChoosingDate > COALESCE((select value from settings where name = 'WinnerBeginDate_{guildId}'), -1) and Channel = @chan group by UserName order by 1 desc, max(ChoosingDate)";
                comma.Parameters.Add(new SQLiteParameter("@chan", guildId));
                SQLiteDataReader dr = comma.ExecuteReader();
                while (dr.Read())
                {
                    result.Add((dr.GetString(1), dr.GetInt32(0)));
                }
                return result;
            }
        }

        public void SetWinner(string user, ulong guildId)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = "insert into Winner (UserName, ChoosingDate, Channel) values (@user, @date, @chan)";
                comma.Parameters.Add(new SQLiteParameter("@user", user));
                comma.Parameters.Add(new SQLiteParameter("@date", DateTime.Now.ToString("yyyyMMddHHmmss")));
                comma.Parameters.Add(new SQLiteParameter("@chan", guildId));
                comma.ExecuteNonQuery();
            }
        }

        public string GetRate(string obj)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = $"select Rate from Rates where Object = @obj";
                comma.Parameters.Add(new SQLiteParameter("@obj", obj));
                string res = comma.ExecuteScalar()?.ToString();
                return res;
            }
        }

        public void SetRate(string obj, string rate)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = "insert into Rates (Object, Rate) values (@obj, @rate)";
                comma.Parameters.Add(new SQLiteParameter("@obj", obj));
                comma.Parameters.Add(new SQLiteParameter("@rate", rate));
                comma.ExecuteNonQuery();
            }
        }

        public bool ShouldGreet(string username)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = $"select GreetingDate from Greetings where UserName = @username";
                comma.Parameters.Add(new SQLiteParameter("@username", username));
                string res = comma.ExecuteScalar()?.ToString();
                if (res == null)
                {
                    return true;
                }

                return res != GetCurrentDateString();
            }
        }

        public void SetGreetingDate(string username)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = "insert or replace into Greetings (UserName, GreetingDate) values (@username, @greetDate)";
                comma.Parameters.Add(new SQLiteParameter("@username", username));
                comma.Parameters.Add(new SQLiteParameter("@greetDate", GetCurrentDateString()));
                comma.ExecuteNonQuery();
            }
        }

        public void SaveCommandUsage(string command, string username)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = "insert into CommandsUsage (Command, UserName, DateTime) values (@command, @username, @dateTime)";
                comma.Parameters.Add(new SQLiteParameter("@command", command));
                comma.Parameters.Add(new SQLiteParameter("@username", username));
                comma.Parameters.Add(new SQLiteParameter("@dateTime", DateTime.Now.ToString("yyyyMMddHHmmss")));
                comma.ExecuteNonQuery();
            }
        }

        public Dictionary<BindKey, string> GetBinds()
        {
            Dictionary<BindKey, string> result = new Dictionary<BindKey, string>();
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = "select ChannelId, UserId, Bind, Content from Binds";
                SQLiteDataReader dr = comma.ExecuteReader();
                while (dr.Read())
                {
                    if (UInt64.TryParse(dr.GetString(0), out ulong channelId) && UInt64.TryParse(dr.GetString(1), out ulong userId))
                    {
                        result.Add(new BindKey { Bind = dr.GetString(2), ChannelId = channelId, UserId = userId }, dr.GetString(3));
                    }
                }
            }

            return result;
        }

        public void AddBind(ulong channelId, ulong userId, string bind, string content)
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = "insert or replace into Binds (ChannelId, UserId, Bind, Content) values (@channelId, @userId, @bind, @content)";
                comma.Parameters.Add(new SQLiteParameter("@channelId", channelId.ToString()));
                comma.Parameters.Add(new SQLiteParameter("@userId", userId.ToString()));
                comma.Parameters.Add(new SQLiteParameter("@bind", bind));
                comma.Parameters.Add(new SQLiteParameter("@content", content));
                comma.ExecuteNonQuery();
            }
        }

        public string? GetGameName()
        {
            using (SQLiteCommand comma = new SQLiteCommand(connection))
            {
                comma.CommandText = "select Value from Settings where Name = 'Game'";
                return comma.ExecuteScalar()?.ToString();
            }
        }

        private string GetCurrentDateString()
        {
            // начнём утро в 6-30
            return DateTime.Now.AddHours(-6).AddMinutes(-30).ShiftToNskTimeZone().ToString("ddMMyyyy");
        }
    }
}

